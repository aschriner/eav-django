# django
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from datetime import date

# this app
from eav import models as eav_models
from eav.forms import BaseDynamicEntityForm


class Schema(eav_models.BaseSchema):
    pass


class Choice(eav_models.BaseChoice):
    schema = models.ForeignKey(Schema, related_name='choices')


class EntityAttributeLinkyDink(eav_models.BaseEntityAttributeLink):
    entity_type = models.ForeignKey(ContentType)
    attribute = models.ForeignKey('AttraHat')


class AttraHat(eav_models.BaseAttribute):
    schema = models.ForeignKey(Schema, related_name='attrs')
    selected_choices = models.ManyToManyField(Choice, blank=True)
    entity_attr_link_through = EntityAttributeLinkyDink


class Entity(eav_models.BaseEntity):
    title = models.CharField(max_length=100)
    price = models.IntegerField(blank=True, null=True, verbose_name='Item price')
    entity_attr_link_through = EntityAttributeLinkyDink

    @classmethod
    def get_schemata_for_model(cls):
        return Schema.objects.all()

    def __unicode__(self):
        return self.title


class AnyObject(models.Model):
    name = models.CharField(max_length=25)


class EntityEAVForm(BaseDynamicEntityForm):
    class Meta:
        model = Entity
