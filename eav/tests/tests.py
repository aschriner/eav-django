from datetime import date
import doctest
import os
import sys
import unittest

from django.core.exceptions import ValidationError
from django.test import TestCase

from eav import forms
from eav.datatypes import NotChoiceError, NotIterableError
from eav.tests import doctests
from eav.tests.models import *


# add doctests to suite
def load_tests(loader, tests, ignore):
    tests.addTests(doctest.DocTestSuite(doctests))
    return tests


class SimpleTest(TestCase):
    def setUp(self):
        """create entities, grabbed from old doctests"""
        colour = Schema.objects.create(title='Colour', datatype=Schema.TYPE_TEXT)
        taste = Schema.objects.create(title='Taste', datatype=Schema.TYPE_TEXT)
        age = Schema.objects.create(title='Age', datatype=Schema.TYPE_FLOAT)
        can_haz = Schema.objects.create(title='I can haz it', datatype=Schema.TYPE_BOOLEAN)
        exp_date = Schema.objects.create(title='Date', datatype=Schema.TYPE_DATE)
        e = Entity.objects.create(title='Apple', colour='green')
        e.taste = 'sour'
        e.age = 112
        e.can_haz = True
        e.exp_date = date(1142, 1, 1)
        e.save()
        self.entity = e

    def test_form_eav_fields(self):
        """Test that the EAV form has fields for static fields and
        dynamic fields"""
        form = EntityEAVForm()
        static_field_names = Entity._meta.get_all_field_names()
        static_field_names.remove('id')
        for f in static_field_names:
            self.assertIn(f, form.fields.keys())
        eav_fields = Entity.get_schema_names(self.entity)
        for f in eav_fields:
            self.assertIn(f, form.fields.keys())

    def test_form_initial(self):
        form = EntityEAVForm(instance=self.entity)
        self.fail("todo")

    def test_TYPE_MANY_returns_iterable(self):
        """
        Redundant with doctests, but it's not working there,
        """
        size = Schema.objects.create(name='size', title='Size', datatype=Schema.TYPE_MANY)
        small = size.choices.create(title='S')
        e = Entity.objects.create(title='T-shirt',
                                  size=[small])
        e = Entity.objects.get(pk=e.pk)
        self.assertEqual(e.size[0], small)

    def test_TYPE_MANY_returns_iterable_when_unset(self):
        """
        Make sure it return an iterable obj (empty list)
        """
        size = Schema.objects.create(name='size', title='Size', datatype=Schema.TYPE_MANY)
        e = Entity.objects.create(title='T-shirt')
        e = Entity.objects.get(pk=e.pk)
        self.assert_(hasattr(e.size, "__iter__"))

    def test_validate_multiple_choice_type(self):
        size = Schema.objects.create(name='size', title='Size', datatype=Schema.TYPE_MANY)
        e = Entity(title='T-shirt')
        e.size = ['wrong choice']
        self.assertRaises(NotChoiceError, e.save)

    def test_validate_multiple_choice_list(self):
        size = Schema.objects.create(name='size', title='Size', datatype=Schema.TYPE_MANY)
        small = size.choices.create(title='S')
        e = Entity(title='T-shirt')
        e.size = small
        self.assertRaises(NotIterableError, e.save)

    def test_validate_single_wrong_choice(self):
        size = Schema.objects.create(name='size', title='Size', datatype=Schema.TYPE_ONE)
        e = Entity(title='T-shirt')
        e.size = ['wrong choice']
        self.assertRaises(NotChoiceError, e.save)

    def test_validate_single_hoice_as_list(self):
        size = Schema.objects.create(name='size', title='Size', datatype=Schema.TYPE_ONE)
        small = size.choices.create(title='S')
        e = Entity(title='T-shirt')
        e.size = [small]
        self.assertRaises(NotChoiceError, e.save)

    def test_long_text(self):
        "test a really long text field."
        dir = os.path.dirname(__file__)
        fullpath = os.path.join(dir, "lorem.txt")
        with open(fullpath, 'r') as lorem:
            lorem_text = lorem.read()
            Schema.objects.create(title="longtext",
                                           name="longtext",
                                           datatype=Schema.TYPE_TEXT)
            e = Entity.objects.create(title='foo', longtext=lorem_text)
            e = Entity.objects.get(pk=e.pk)
            self.assertEqual(e.longtext, lorem_text)

    def test_generic_obj_set_single_choice(self):
        object1 = AnyObject.objects.create(name="foo")
        choose_obj = Schema.objects.create(name='thing', title='thing',
                                           datatype=Schema.TYPE_SINGLE_OBJ,
                                           choice_object=ContentType.objects.get_for_model(object1))
        e = Entity.objects.create(title='T-shirt', thing=object1)
        e = Entity.objects.get(pk=e.pk)
        self.assertEqual(e.thing, object1)

    def test_generic_obj_set_multiple_choices(self):
        object1 = AnyObject.objects.create(name="foo")
        object2 = AnyObject.objects.create(name="bar")
        choose_obj = Schema.objects.create(name='things', title='things',
                                           datatype=Schema.TYPE_MANY_OBJ,
                                           choice_object=ContentType.objects.get_for_model(object1))
        e = Entity.objects.create(title='T-shirt', things=[object1, object2])
        e = Entity.objects.get(pk=e.pk)
        self.assertEqual(list(e.things), [object1, object2])

    def test_obj_choice_attr_wrong_choice(self):
        for i, dtype in enumerate([Schema.TYPE_SINGLE_OBJ, Schema.TYPE_MANY_OBJ]):
            object1 = AnyObject.objects.create(name="foo")
            Schema.objects.create(title="thing%s" % i,
                   datatype=dtype,
                   choice_object=ContentType.objects.get_for_model(object1))
            kwargs = {"title": "T-shirt",
                     "thing%s" % i: "STRINGTHING"}
            self.assertRaises(TypeError, Entity.objects.create,
                              **kwargs)

    def test_not_iterable_for_many_obj_choice(self):
        object1 = AnyObject.objects.create(name="foo")
        Schema.objects.create(title="thing",
                   datatype=Schema.TYPE_MANY_OBJ,
                   choice_object=ContentType.objects.get_for_model(object1))
        kwargs = {"title": "T-shirt",
                 "thing": object1}
        self.assertRaises(TypeError, Entity.objects.create,
                          **kwargs)

    def test_schema_clean_type_obj(self):
        # create a schema with datatype=type_obj something, but without setting
        # the object_ct field
        schema = Schema(title="foo", datatype=Schema.TYPE_MANY_OBJ)
        self.assertRaises(ValidationError, schema.clean)

    def test_delete_entity_deletes_eal(self):
        assert EntityAttributeLinkyDink.objects.count() == EntityAttributeLinkyDink.objects.filter(entity_id=self.entity.id).count()
        self.entity.delete()
        self.assertEqual(EntityAttributeLinkyDink.objects.count(), 0)

    def test_filters(self):
        """
        ##
        ## filtering
        ##
        
        >>> Entity.objects.filter(title='Apple')
        [<Entity: Apple>]
        >>> Entity.objects.filter(colour='yellow')
        [<Entity: Apple>]
        >>> Entity.objects.filter(colour='yellow', title='Apple')
        [<Entity: Apple>]
        
        
        >>> Entity.objects.filter(taste='sweet')
        [<Entity: Apple>, <Entity: Orange>, <Entity: Tangerine>]
        >>> Entity.objects.filter(colour='orange', size__in=[small, large])
        [<Entity: Tangerine>, <Entity: Old Dog>]
        
        >>> Entity.objects.create(title='Orange', colour='orange', taste='sweet', size=medium)
        <Entity: Orange>
        >>> Entity.objects.create(title='Tangerine', colour='orange', taste='sweet', size=small)
        <Entity: Tangerine>
        >>> Entity.objects.create(title='Old Dog', colour='orange', taste='bitter', size=large)
        <Entity: Old Dog>
        
        #
        # exclude() fetches objects that either have given attribute(s) with other values
        # or don't have any attributes for this schema at all:
        #
        
        >>> Entity.objects.exclude(size=small)
        [<Entity: Apple>, <Entity: Cane>, <Entity: Orange>, <Entity: Old Dog>]
        >>> Entity.objects.exclude(taste='sweet')
        [<Entity: T-shirt>, <Entity: Cane>, <Entity: Old Dog>]
        >>> Entity.objects.filter(size=large) & Entity.objects.exclude(colour='orange')
        [<Entity: T-shirt>]
        >>> Entity.objects.filter(size=large) & Entity.objects.filter(colour='orange')
        [<Entity: Old Dog>]
        
        """
        pass

    def test_choice_schema(self):
        size = Schema.objects.create(name='size', title='Size', datatype=Schema.TYPE_MANY)
        large = size.choices.create(title='L')
        self.assertEqual(large.schema, size)

class CacheTests(TestCase):

    def test_createentity_addschema_assignattrs(self):
        # when creating the entity first, then adding schemas, then assigning attrs
        # make sure attrs get saved

        # testing cache/save behavior
        entity = Entity.objects.create(title="entity1")
        col = Schema.objects.create(title='Colour', datatype=Schema.TYPE_TEXT)

        assert not AttraHat.objects.exists()
        entity.colour = "aquamarine"
        assert not AttraHat.objects.exists()
        entity.save()
        assert AttraHat.objects.exists()
        entity = Entity.objects.get(pk=entity.pk)
        self.assertEqual(entity.colour, "aquamarine")


class FormTests(TestCase):
    def test_form_fields(self):
        # test the form field (widget, rendering?) for each datatype

        for dtype in Schema.DATATYPE_CHOICES[0:5]:
            Schema.objects.create(title="foo",
                                  datatype=dtype[0])
            e = Entity.objects.create()
            form = EntityEAVForm(instance=e)


        self.fail("Test What?")

    def test_single_obj_field(self):
        object1 = AnyObject.objects.create(name="foo")
        choose_obj = Schema.objects.create(name='thing', title='thing',
                                           datatype=Schema.TYPE_SINGLE_OBJ,
                                           choice_object=ContentType.objects.get_for_model(object1))
        e = Entity.objects.create(title='T-shirt', thing=object1)
        e = Entity.objects.get(pk=e.pk)
        form = EntityEAVForm(instance=e)
        qs = form.fields['thing'].queryset
        self.assertEqual(qs.model, AnyObject)
