# -*- coding: utf-8 -*-
#
#    EAV-Django is a reusable Django application which implements EAV data model
#    Copyright © 2009—2010  Andrey Mikhaylenko
#
#    This file is part of EAV-Django.
#
#    EAV-Django is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    EAV-Django is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with EAV-Django.  If not, see <http://gnu.org/licenses/>.

"""
Tests
~~~~~

##
## Currently in the process of porting these tests to unittests
##


##
## basic EAV
##

>>> colour = Schema.objects.create(title='Colour', datatype=Schema.TYPE_TEXT)
>>> colour
<Schema: Colour (text)>
>>> colour.name              #  <-- automatically generated from title
u'colour'
>>> taste = Schema.objects.create(title='Taste', datatype=Schema.TYPE_TEXT)
>>> age = Schema.objects.create(title='Age', datatype=Schema.TYPE_FLOAT)
>>> can_haz = Schema.objects.create(title='I can haz it', datatype=Schema.TYPE_BOOLEAN)
>>> can_haz.name
u'i_can_haz_it'
>>> exp_date = Schema.objects.create(title='Date', datatype=Schema.TYPE_DATE)
>>> e = Entity.objects.create(title='Apple', colour='green')
>>> e.title
'Apple'
>>> e.colour
'green'
>>> colour.datatype = Schema.TYPE_FLOAT
>>> colour.save()
Traceback (most recent call last):
 ...
SchemaChangeError: Schema 'Colour (number)' already has attributes attached; you cannot change its datatype
>>> e.saved_attrs.all()
[<AttraHat: Colour "green">]
>>> e.taste = 'sweet'
>>> e.taste
'sweet'
>>> e.saved_attrs.all()
[<AttraHat: Colour "green">]
>>> e.colour = 'yellow'
>>> e.save()
>>> e.saved_attrs.all()
[<AttraHat: Colour "yellow">, <AttraHat: Taste "sweet">]
>>> e.colour = None
>>> e.saved_attrs.all()  # checks db, but we haven't saved yet
[<AttraHat: Colour "yellow">, <AttraHat: Taste "sweet">]
>>> e.save()
>>> e.saved_attrs.all()  # should return Attr with None, or no Attr?
[<AttraHat: Taste "sweet">]
>>> e.colour = 'yellow'
>>> e.save()
>>> e.date = 12
>>> e.date
12
>>> e.save()
Traceback (most recent call last):
    ...
TypeError: expected string or buffer
>>> e = Entity.objects.get(pk=e.pk)
>>> e.date
>>> e.date = date(1986, 1, 13)
>>> e.date
datetime.date(1986, 1, 13)
>>> e.save()
>>> e = Entity.objects.get(pk=e.pk)
>>> e.date
datetime.date(1986, 1, 13)

##
## many choices
##

>>> size = Schema.objects.create(name='size', title='Size', datatype=Schema.TYPE_MANY)
>>> small  = size.choices.create(title='S')
>>> medium = size.choices.create(title='M')
>>> large  = size.choices.create(title='L')
>>> small
<Choice: S>
>>> e = Entity(title='T-shirt')
>>> e.size = [small]
>>> e.save()
>>> e2 = Entity.objects.get(pk=e.pk)
>>> e2.size
[<Choice: S>]
>>> e2.size = [medium, large]
>>> e2.save()
>>> e2.size = [large, medium]
>>> e2.save()
>>> e3 = Entity.objects.get(pk=e.pk)
>>> e3.size
[<Choice: L>, <Choice: M>]
>>> AttraHat.objects.all().values_list("id", flat=True)
[2, 3, 4, 5]
>>> AttraHat.objects.all().order_by('schema', 'selected_choices__id')
[<AttraHat: Colour "yellow">, <AttraHat: Date "1986-01-13">, <AttraHat: Size "[<Choice: L>, <Choice: M>]">, <AttraHat: Size "[<Choice: L>, <Choice: M>]">, <AttraHat: Taste "sweet">]
>>> e2.size = [small, large]
>>> e2.save()
>>> e3 = Entity.objects.get(pk=e.pk)
>>> e3.size
[<Choice: L>, <Choice: S>]
>>> AttraHat.objects.all().values_list("id", flat=True)
[2, 3, 4, 5]
>>> AttraHat.objects.all().order_by('schema', 'selected_choices__id')
[<AttraHat: Colour "yellow">, <AttraHat: Date "1986-01-13">, <AttraHat: Size "[<Choice: L>, <Choice: S>]">, <AttraHat: Size "[<Choice: L>, <Choice: S>]">, <AttraHat: Taste "sweet">]
>>> e3.save()
>>> AttraHat.objects.all().order_by('schema', 'selected_choices__id')
[<AttraHat: Colour "yellow">, <AttraHat: Date "1986-01-13">, <AttraHat: Size "[<Choice: L>, <Choice: S>]">, <AttraHat: Size "[<Choice: L>, <Choice: S>]">, <AttraHat: Taste "sweet">]

##
## single choice
##

>>> protein = Schema.objects.create(name='protein', title='Protein', datatype=Schema.TYPE_ONE)
>>> egg_albumen = protein.choices.create(title='Egg Albumen')
>>> gluten = protein.choices.create(title='Gluten')
>>> lean_meat = protein.choices.create(title='Lean Meat')
>>> egg_albumen
<Choice: Egg Albumen>
>>> e = Entity(title='Cane')
>>> e.save()
>>> e.protein
>>> e.protein = egg_albumen
>>> e.save()
>>> e2 = Entity.objects.get(pk=e.pk)
>>> e2.protein
<Choice: Egg Albumen>
>>> e3 = Entity.objects.get(pk=e.pk)
>>> e3.protein
<Choice: Egg Albumen>


##
## combined
##

>>> Entity.objects.create(title='Orange', colour='orange', taste='sweet', size=[medium])
<Entity: Orange>
>>> Entity.objects.create(title='Tangerine', colour='orange', taste='sweet', size=[small])
<Entity: Tangerine>
>>> Entity.objects.create(title='Old Dog', colour='orange', taste='bitter', size=[large])
<Entity: Old Dog>

--------------------------
"""
from eav.tests.models import *