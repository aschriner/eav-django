# -*- coding: utf-8 -*-
#
#    EAV-Django is a reusable Django application which implements EAV data model
#    Copyright Â© 2009â€”2010  Andrey Mikhaylenko
#
#    This file is part of EAV-Django.
#
#    EAV-Django is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    EAV-Django is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with EAV-Django.  If not, see <http://gnu.org/licenses/>.
"""
Models
~~~~~~
"""
# django
# 3rd-party
# from view_shortcuts.decorators import cached_property
# this app

import logging

from autoslug.fields import AutoSlugField
from autoslug.settings import slugify
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError, ObjectDoesNotExist, \
    MultipleObjectsReturned
from django.db import transaction
from django.db.models import BooleanField, CharField, DateField, FloatField, \
    ForeignKey, IntegerField, Model, NullBooleanField, TextField, \
    PositiveIntegerField
from django.utils.translation import ugettext_lazy as _

import datatypes
from managers import BaseEntityManager


logger = logging.getLogger(__name__)


class EAVIntegrityError(Exception):
    pass

__all__ = ['BaseAttribute', 'BaseChoice', 'BaseEntity', 'BaseSchema']


class SchemaChangeError(Exception):
    pass


def slugify_attr_name(name):
    return slugify(name.replace('_', '-')).replace('-', '_')


def get_entity_lookups(entity):
    entitylinkthrough = entity.entity_attr_link_through.__name__.lower()
    ctype = ContentType.objects.get_for_model(entity)
    return {'{0}__entity_type'.format(entitylinkthrough): ctype,
            '{0}__entity_id'.format(entitylinkthrough): entity.pk}


class BaseSchema(Model):
    """
    Metadata for an attribute.
    """
    TYPE_TEXT = 'text'
    TYPE_FLOAT = 'float'
    TYPE_DATE = 'date'
    TYPE_BOOLEAN = 'bool'
    TYPE_ONE = '1chc'
    TYPE_MANY = '2+chc'
    TYPE_SINGLE_OBJ = '1obj'
    TYPE_MANY_OBJ = '2+obj'

    DATATYPE_CHOICES = (
        (TYPE_TEXT, _('text')),
        (TYPE_FLOAT, _('number')),
        (TYPE_DATE, _('date')),
        (TYPE_BOOLEAN, _('boolean')),
        (TYPE_ONE, _('single choice (predefined choices)')),
        (TYPE_MANY, _('multiple choice (predefined choices)')),
        (TYPE_SINGLE_OBJ, _('single choice (dynamic choices) ')),
        (TYPE_MANY_OBJ, _('multiple choices (dynamic choices)'))
    )

    _datatype_classes = {
        TYPE_TEXT: datatypes.Text,
        TYPE_FLOAT: datatypes.Float,
        TYPE_DATE: datatypes.Date,
        TYPE_BOOLEAN: datatypes.Boolean,
        TYPE_ONE: datatypes.SingleChoice,
        TYPE_MANY: datatypes.MultipleChoice,
        TYPE_SINGLE_OBJ: datatypes.SingleObject,
        TYPE_MANY_OBJ: datatypes.ManyObjects
                         }
    title = CharField(_('title'), max_length=250, help_text=_('user-friendly attribute name'))
    name = AutoSlugField(_('name'), max_length=250, populate_from='title',
                             editable=True, blank=True, slugify=slugify_attr_name)
    question_text = CharField(max_length=250,
                              help_text=_('Text to be used for html form <label>'),
                              blank=True)
    help_text = CharField(_('help text'), max_length=250, blank=True,
                          help_text=_('short description for administrator'))
    datatype = CharField(_('data type'), max_length=5, choices=DATATYPE_CHOICES)
    choice_object = ForeignKey(ContentType,
                           help_text="Which model should be used for choices?",
                           null=True, blank=True)

    required = BooleanField(_('required'), default=False)
    searched = BooleanField(_('include in search'), default=False)  # i.e. full-text search? mb for text only
    filtered = BooleanField(_('include in filters'), default=False)
    sortable = BooleanField(_('allow sorting'), default=False)

    class Meta:
        abstract = True
        verbose_name, verbose_name_plural = _('schema'), _('schemata')
        ordering = ['title', "id"]

    def clean(self):
        if self.datatype in [self.TYPE_MANY_OBJ, self.TYPE_SINGLE_OBJ]:
            if not self.choice_object:
                raise ValidationError("You must specify the related "
                  "'choice_object' if the datatype is either single "
                  "or multiple *object* choice.")
        return super(BaseSchema, self).clean()

    def __unicode__(self):
        return u'%s (%s)%s' % (self.title, self.get_datatype_display(),
                                u' %s' % _('required') if self.required else '')

    @property
    def datatype_class(self):
        return self._datatype_classes[self.datatype]

    def get_choices(self):
        """
        Returns a queryset of choice objects bound to this schema.
        """
        return self.choices.all()  # expects related_name="choices" on Choice FK

    def get_attribute_obj(self, entity):
        """Return Attribute object"""
        try:
            return self.attrs.get(**get_entity_lookups(entity))
        except ObjectDoesNotExist:
            return None
        except MultipleObjectsReturned:
            attrs = self.attrs.filter(**get_entity_lookups(entity))
            raise EAVIntegrityError("Somehow there are multiple attrs for %s.%s. They are%s" % (entity, self, attrs))

    def save_attr(self, entity, value):
        """
        Saves given EAV attribute with given value for given entity.
        `Value` will look a little different depending on its datatype.
        datatypes are smart enough to know how to handle their own values.

        End goal:
        Attribute object should exist for `value`
        Attribute should be linked to `entity`

        There should only ever be one Attribute for Entity/Schema!!!
        """
        self.datatype_class(self).save_value(entity, value)

    def save(self, *args, **kwargs):
        if self.pk and self.attrs.all().exists():
            orig = self.__class__.objects.get(pk=self.pk)
            if orig.datatype != self.datatype:
                raise SchemaChangeError("Schema '%s' already has attributes attached; you cannot change its datatype" % self)
        return super(BaseSchema, self).save(*args, **kwargs)


class BaseEntityAttributeLink(Model):
    # provide FK to ContentType; use "limit_choices_to" in subclass if you like
    entity_type = NotImplementedError("Must provide a FK to ContentType")
    entity_id = IntegerField()
    entity = generic.GenericForeignKey(ct_field="entity_type",
                                       fk_field='entity_id')

    attribute = NotImplementedError(
                            "Must provide a FK to BaseAttribute subclass")

    def validate_unique(self, *args, **kwargs):
        """
        There are two important validation concerns:
        1. There should be only one link between an Entity instance and 
        Attribute instance.  (This is handled at SQL level by ManyToMany)
        2. There should be only one triplet Entity-Schema-Attribute.  This 
        method checks that for a given Entity and Schema, there is only
        one Attribute.  
        """
        super(BaseEntityAttributeLink, self).validate_unique(*args, **kwargs)
        # this should find None
        existing_attr = self.attribute.schema.get_attribute(self.entity)
        if existing_attr:
            raise ValidationError(
                "Entity %s - Schema %s already has Attribute %s" % (
                                                self.entity,
                                                self.attribute.schema,
                                                existing_attr))

    class Meta:
        abstract = True
        unique_together = ["entity_type", "entity_id", "attribute"]



class BaseEntity(Model):
    """
    Entity, the "E" in EAV. This model is abstract and must be subclassed.
    See tests for examples.
    """
    objects = BaseEntityManager()

    entity_attr_link_through = NotImplemented  # must be subclass of BaseEntityAttributeLink (class, not FK)

    @property
    def saved_attrs(self):
        """
        Return associated Attribute objects.  They may not be synced with the
        current Entity's values if the Entity has not been saved.
        """
        entity_lookups = get_entity_lookups(self)
        schemas = self.get_schemata()
        attr_model = self.entity_attr_link_through.attribute.field.rel.to
        return attr_model.objects.filter(schema__in=schemas, **entity_lookups)

    class Meta:
        abstract = True

    @transaction.atomic
    def save(self, force_eav=False, **kwargs):
        """
        Saves entity instance and creates/updates related attribute instances.

        :param eav: if True (default), EAV attributes are saved along with entity.
        """

        # save w/ regular django model method
        super(BaseEntity, self).save(**kwargs)

        # create/update EAV attributes
        for schema in self.get_schemata(check_cache=False):
            value = getattr(self, schema.name, None)
            schema.save_attr(self, value)

    def __getattr__(self, name):
        if not name.startswith('_'):
            if name in self.get_schema_names():
                schema = self.get_schema(name)
                attr = schema.get_attribute_obj(self)
                logger.debug("ENTITY: {0} - SCHEMA: {1} - ATTRIBUTE: {2}".format(self, schema, attr))
                if attr is None:
                    return schema.datatype_class.none_value
                else:
                    return attr.value
        raise AttributeError('%s does not have attribute named "%s".' %
                             (self._meta.object_name, name))

    @classmethod
    def get_schemata_for_model(cls):
        return NotImplementedError('BaseEntity subclasses must define method '
                                   '"get_schemata_for_model" which returns a '
                                   'QuerySet for a BaseSchema subclass.')

    def get_schemata_for_instance(self, qs):
        return qs

    def get_schemata(self, check_cache=True):
        if check_cache:
            if hasattr(self, '_schemata_cache') and self._schemata_cache is not None:
                return self._schemata_cache
        all_schemata = self.get_schemata_for_model().select_related()
        self._schemata_cache = self.get_schemata_for_instance(all_schemata)
        self._schemata_cache_dict = dict((s.name, s) for s in self._schemata_cache)
        return self._schemata_cache

    def get_schema_names(self):
        if not hasattr(self, '_schemata_cache_dict'):
            self.get_schemata()
        return self._schemata_cache_dict.keys()

    def get_schema(self, name):
        if not hasattr(self, '_schemata_cache_dict'):
            self.get_schemata()
        return self._schemata_cache_dict[name]

    def get_schema_by_id(self, schema_id):
        for schema in self.get_schemata():
            if schema.pk == schema_id:
                return schema

    def check_eav_allowed(self):
        """
        Returns True if entity instance allows EAV attributes to be attached.

        Can be useful if some external data is required to determine available
        schemata and that data may be missing. In such cases this method should
        be overloaded to check whether the data is available.
        """
        return True

    def is_valid(self):
        "Returns True if attributes and their values conform with schema."

        raise NotImplementedError()

        '''
        schemata = self.rubric.schemata.all()
        return all(x.is_valid for x in self.attributes)
        # 1. check if all required attributes are present
        for schema in schemata:
            pass
        # 2. check if all attributes have appropriate values
        for schema in schemata:
            pass
        return True
        '''


class BaseChoice(Model):
    """ Base class for choices.  Concrete choice class must overload the
    `schema` attribute.
    """
    title = CharField(max_length=100)
    schema = NotImplemented  # must have related name = "choices"

    class Meta:
        abstract = True
        ordering = ('title',)

    def __unicode__(self):
        return u"%s" % self.title  # u'%s "%s"' % (self.schema.title, self.title)


class BaseAttribute(Model):
    """ Base class for choices.  Concrete choice class must overload the
    `schema` and `choice` attributes.
    """
#     value_text = TextField(blank=True, null=True)
    # get field info from datatype classes
    value_text = BaseSchema._datatype_classes[BaseSchema.TYPE_TEXT].field
    value_float = BaseSchema._datatype_classes[BaseSchema.TYPE_FLOAT].field
    value_date = BaseSchema._datatype_classes[BaseSchema.TYPE_DATE].field
    value_bool = BaseSchema._datatype_classes[BaseSchema.TYPE_BOOLEAN].field

    schema = NotImplemented  # must be FK, and have related_name="attrs"
    selected_choices = NotImplemented  # must be ManyToMany(blank=True) to Choice
    entity_attr_link_through = NotImplemented  # must be subclass of BaseEntityAttributeLink (class, not FK)

    class Meta:
        abstract = True
        verbose_name, verbose_name_plural = _('attribute'), _('attributes')

    def __unicode__(self):
#         import pdb;pdb.set_trace()
        return u'%s "%s"' % (self.schema.title, self.value)

    def save(self, *args, **kwargs):
        if self.pk and self.value is None:
            self.delete()
        else:
            super(BaseAttribute, self).save(*args, **kwargs)

    def _get_value(self):
        return self.schema.datatype_class(self.schema).get_value(self)

    def _set_value(self, new_value):
            setattr(self, 'value_%s' % self.schema.datatype, new_value)

    value = property(_get_value, _set_value)
