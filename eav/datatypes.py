import ast
import logging

from django.contrib.admin.widgets import AdminDateWidget, FilteredSelectMultiple, \
    AdminRadioSelect  # , RelatedFieldWidgetWrapper
from django.core.exceptions import ValidationError
from django.db import models
from django.forms import BooleanField, CharField, CheckboxSelectMultiple, \
    DateField, FloatField, ModelForm, ModelChoiceField, ModelMultipleChoiceField, \
    ValidationError


logger = logging.getLogger(__name__)

class NotChoiceError(TypeError):
    pass

class NotIterableError(TypeError):
    pass


class DataType(object):
    form_field_kwargs = {}
    form_field = NotImplemented
    none_value = None  # returned when no Attribute object is found

    def __init__(self, schema):
        self.schema = schema

    def get_value(self, attr_obj):
        raise NotImplementedError()

    def save_value(self, entity, value):
        """
        Create Attribute instance and EntityAttributeLink or 
        just update Attribute.
        """
        logger.debug("HANDLING %s on %s.%s" % (unicode(value), entity, self.schema))
        attr_obj = self._get_attribute_obj(entity)
        if attr_obj is None:
            if value is None:
                return  # nothing to do
            else:  # need to create attr_obj and link to entity
                logger.debug("CREATING %s on %s.%s" % (value, entity, self.schema))
                attr_obj = self._create_attr_obj(entity, value)
                self._link_entity_to_attr(entity, attr_obj)
        else:
            if attr_obj._get_value() != value:
                logger.debug("UPDATING %s on %s.%s" % (unicode(value), entity, self.schema))
                logger.debug("old val = %s, new val = %s" % (unicode(attr_obj._get_value()), unicode(value)))
                self._update_attr_obj(attr_obj, value)
        return attr_obj

    def _create_attr_obj(self, entity, value):
        attr_model = self.schema.attrs.model
        attr = attr_model(schema=self.schema)
        self._validate_value(value)
        self._set_value(attr, value)
        attr.save()
        return attr

    def _link_entity_to_attr(self, entity, attr_obj):
        entity.entity_attr_link_through(entity=entity, attribute=attr_obj).save()

    def _update_attr_obj(self, attr_obj, value):
        if not value:
            attr_obj.delete()
        else:
            self._validate_value(value)
            self._set_value(attr_obj, value)
            attr_obj.save()

    def _get_attribute_obj(self, entity):
        """Return Attribute object"""
        return self.schema.get_attribute_obj(entity)

    def _set_value(self, attr_obj, value):
        raise NotImplementedError

    def _validate_value(self, value):
        """For extra validation, beyond Django model field validation."""
        pass

    def get_form_field_kwargs(self):
        return self.form_field_kwargs


class Text(DataType):
    field = models.TextField(blank=True, null=True)
    form_field = CharField

    def _set_value(self, attr_obj, value):
        attr_obj.value_text = value

    def get_value(self, attr_obj):
        return attr_obj.value_text


class Float(DataType):
    form_field = FloatField
    field = models.FloatField(blank=True, null=True)

    def _set_value(self, attr_obj, value):
        attr_obj.value_float = value

    def get_value(self, attr_obj):
        return attr_obj.value_float


class Date(DataType):
    form_field = DateField
    form_field_kwargs = {'widget': AdminDateWidget}
    field = models.DateField(blank=True, null=True)

    def _set_value(self, attr_obj, value):
        attr_obj.value_date = value

    def get_value(self, attr_obj):
        return attr_obj.value_date


class Boolean(DataType):
    form_field = BooleanField
    field = models.NullBooleanField(blank=True)

    def _set_value(self, attr_obj, value):
        attr_obj.value_bool = value

    def get_value(self, attr_obj):
        return attr_obj.value_bool


class ChoiceDataTypeMixin(object):

    def get_form_field_kwargs(self):
        return {"queryset": self.schema.get_choices()}

    def _create_attr_obj(self, entity, value):
        """
        have to save obj before we can start attaching m2m
        """
        attr_model = self.schema.attrs.model
        attr = attr_model(schema=self.schema)
        attr.save()
        self._validate_value(value)
        self._set_value(attr, value)
        attr.save()
        return attr


class SingleChoice(ChoiceDataTypeMixin, DataType):
    form_field = ModelChoiceField

    def get_form_field_kwargs(self):
        opts = super(SingleChoice, self).get_form_field_kwargs()
        add = {'widget': AdminRadioSelect,
               'empty_label': None if self.schema.required else u"---------"}
        opts.update(add)
        return opts

    def _validate_value(self, value):
        from eav.models import BaseChoice
        """Value could be list or single obj."""
        if isinstance(value, list):
            raise NotChoiceError("""Need a single choice field; got a list: %s""" % value)
        if not isinstance(value, BaseChoice):
            raise NotChoiceError("""Value must be Choice instance, got: %s""" % value)

    def get_value(self, attr_obj):
        return attr_obj.selected_choices.first()

    def _set_value(self, attr_obj, value):
        attr_obj.selected_choices = [value]


class MultipleChoice(ChoiceDataTypeMixin, DataType):
    form_field = ModelMultipleChoiceField
    none_value = []

    def get_form_field_kwargs(self):
        opts = super(MultipleChoice, self).get_form_field_kwargs()
        if len(self.schema.get_choices()) <= 5:
            widget = CheckboxSelectMultiple
        else:
            widget = FilteredSelectMultiple(self.schema.title,
                                            is_stacked=False)
        opts.update({"widget": widget, })
                # "queryset": self.schema.get_choices()})
        return opts

    def _validate_value(self, value):
        from eav.models import BaseChoice
        if not  hasattr(value, '__iter__'):
            raise NotIterableError("""Value must be iterable of Choice instances, got %s""" % value)
        for v in value:
            if not isinstance(v, BaseChoice):
                raise NotChoiceError("""Value must contain Choice instances, got %s""" % value)

    def get_value(self, attr_obj):
        return attr_obj.selected_choices.all()

    def _set_value(self, attr_obj, value):
        attr_obj.selected_choices = value


class GenericObjectDataTypeMixin(object):
    def get_form_field_kwargs(self):
        return {"queryset": self.schema.choice_object.model_class().objects.filter()}


class SingleObject(GenericObjectDataTypeMixin, DataType):
    form_field = ModelChoiceField

    def get_form_field_kwargs(self):
        opts = super(SingleObject, self).get_form_field_kwargs()
        add = {'widget': AdminRadioSelect,
               'empty_label': None if self.schema.required else u"---------"}
        opts.update(add)
        return opts

    def _validate_value(self, value):
        if not isinstance(value, self.schema.choice_object.model_class()):
            raise NotChoiceError("""Value must be an instance of '%s', got %s""" \
                                 % (self.schema.choice_object, value))

    def _set_value(self, attr_obj, value):
        attr_obj.value_text = value.pk  # store as text in case it's non-integer

    def get_value(self, attr_obj):
        return self.schema.choice_object.model_class().objects.get(
                                                          pk=attr_obj.value_text)


class ManyObjects(GenericObjectDataTypeMixin, DataType):
    form_field = ModelMultipleChoiceField
    none_value = []

    def get_form_field_kwargs(self):
        opts = super(ManyObjects, self).get_form_field_kwargs()
        if opts['queryset'].count() <= 5:
            widget = CheckboxSelectMultiple
        else:
            widget = FilteredSelectMultiple(self.schema.title,
                                            is_stacked=False)
        opts.update({"widget": widget, })
        return opts

    def _validate_value(self, value):
        if not  hasattr(value, '__iter__'):
            raise NotIterableError("Value must be iterable of model instances, "
                                   "got '%s'" % value)
        for v in value:
            if not isinstance(v, self.schema.choice_object.model_class()):
                raise NotChoiceError("Value must contain instances of '%s', "
                                     "got %s" % (self.schema.choice_object,
                                                 value))

    def _set_value(self, attr_obj, value):
        attr_obj.value_text = unicode([v.pk for v in value])

    def get_value(self, attr_obj):
        id_list = ast.literal_eval(attr_obj.value_text)
        return self.schema.choice_object.model_class().objects.filter(
                                                          pk__in=id_list)
